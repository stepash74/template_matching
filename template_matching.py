import cv2
import numpy as np
import os

values_templates = ('value/2.png', 'value/3.png', 'value/4.png', 'value/5.png', 'value/6.png', 'value/7.png',
                    'value/8.png', 'value/9.png', 'value/10.png', 'value/jack.png', 'value/queen.png',
                    'value/king.png', 'value/ace.png')

big_suits_templates = ('suit/hearts.png', 'suit/clubs.png', 'suit/diamonds.png', 'suit/spades.png')

small_suits_templates = ('suit/hearts_small.png', 'suit/clubs_small.png', 'suit/diamonds_small.png',
                         'suit/spades_small.png')

values = ('2', '3', '4', '5', '6', '7', '8', '9', '10', 'jack', 'queen', 'king', 'ace')

suits = ('hearts', 'clubs', 'diamonds', 'spades')

suits_statistics = [0, 0, 0, 0]


#   распознование карты img:входящее изображение
def card_recognition(img):
    img_rgb = cv2.imread(img)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)

    suit_recognition(img_gray, img)
    value_recognition(img_gray, img)


#   распознование номинала карты img:входящее изображение, img_name:имя изображения
def value_recognition(img, img_name):
    sub_img = img[0:363, 0:500]

    value_index = 0
    for val in values_templates:
        template = cv2.imread(val, 0)
        w, h = template.shape[::-1]

        res = cv2.matchTemplate(sub_img, template, cv2.TM_CCOEFF_NORMED)
        threshold = 0.90
        loc = np.where(res >= threshold)

        loop_counter = 0
        for pt in zip(*loc[::-1]):

            cv2.rectangle(sub_img, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 1)
            if loop_counter == 0:
                try:
                    img_dir, img_name_split = img_name.split('/')
                    print(img_name_split + ': ' + values[value_index])
                    loop_counter += 1
                except ValueError:
                    print(img_name)
                    loop_counter += 1
        value_index += 1


#   распознавание масти карты img:входящее изображение, img_name:имя изображения
def suit_recognition(img, img_name):
    card_enum(big_suits_templates, img, img_name, suits)
    card_enum(small_suits_templates, img, img_name, suits)


#  перечисление и сопоставление всех карт с шаблонами. необходим для соотвествия DRY. templates: массив шаблонов масти,
#  img_name:имя изображения, suits: перечисление мастей
def card_enum(templates, img, img_name, suits):
    suit_index = 0
    for val in templates:
        template = cv2.imread(val, 0)
        w, h = template.shape[::-1]

        res = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)
        threshold = 0.90
        loc = np.where(res >= threshold)

        loop_counter = 0
        for pt in zip(*loc[::-1]):
            cv2.rectangle(img, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 1)
            if loop_counter == 0:
                try:
                    img_dir, img_name = img_name.split('/')
                    print(img_name + ': ' + suits[suit_index])
                    suits_statistics[suit_index] += 1
                    loop_counter += 1
                except ValueError:
                    print(img_name)
                    loop_counter += 1
        suit_index += 1


#   вычисление процентной доли
def calculate_percentage(n, total):
    result = (n/total) * 100
    return result


#   вывод статистики по процентному количеству карт
def show_stat():
    suit_count = 0
    onlyfiles = len(next(os.walk("Cards/"))[2])  # dir is your directory path as string
    print('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    for suit in suits_statistics:
        suit_percent = round(calculate_percentage(suit, onlyfiles), 2)
        print(str(suit_percent) + "%: " + str(suits[suit_count]))
        suit_count += 1
    print('Total cards: ' + str(onlyfiles))


#  точка входа в программу
cards = os.listdir('Cards/')
for card in cards:
    card_path = 'Cards/' + card
    card_recognition(card_path)

show_stat()



